gulp-starter
============

[ ![Codeship Status for K0TT/gulp_starter](https://app.codeship.com/projects/edba7a20-d39a-0135-dd3e-0eadded0d45f/status?branch=master)](https://app.codeship.com/projects/262840)

[![codecov](https://codecov.io/gh/K0TT/gulp_starter/branch/master/graph/badge.svg)](https://codecov.io/gh/K0TT/gulp_starter)

Includes the following tools, tasks, and workflows:

- Documentation tools for SASS and Javascript
- ESlint
- ES6 transpiling with webpack
- Flow: static type checking for javascript
- Jade templates
- Ava test runner
- Code coverage (Codecov)
- SASS/Stylus compilation


Installation
-------------

You need node and npm to install. 

    npm install

You can also use yarn

    yarn install

Usage
-----

1. cd to the root of your project. 
2. From your terminal, run the following command

    echo "STATIC_DIR=src" >> .env

Replace "src" with the destination of your static folder

Now simply run the gulp command in the base directory

    gulp

Configuration
-------------

All paths and plugin settings have been abstracted into a centralized config object in `gulp/config.js`. Adapt the paths and settings to the structure and needs of your project.

ENV Variables
-------------

The test runner calls gulp-codecov to upload test reports to Codecov server. 
Make sure you export the CODECOV_TOKEN before running tests or remove 'coverage'
from the test task in gulp/tasks if you don't want Codecov uploads.
Note that you can also store CODECOV_TOKEN in an .env file at the root of your project

```echo "CODECOV_TOKEN=token" >> .env```
