/**
 * Generates documentation
 *     js:     jsDoc
 *     scss:   sassdoc
 */
import gulp from 'gulp';
import sassdoc from 'sassdoc';
import gulpDocumentation from 'gulp-documentation';
import * as config from '../config.js';

gulp.task('jsdoc', (cb) => {
    return gulp.src(config.js.files)
    .pipe(gulpDocumentation('html'))
    .pipe(gulp.dest(config.docs.js.src))
});

gulp.task('sassdoc', () => {
    return gulp.src(config.scss.files)
           .pipe(sassdoc({
               dest: config.docs.scss.src, 
               verbose:true
           }))
});

gulp.task('docs', ['jsdoc', 'sassdoc']);
