'use strict';

import gulp from 'gulp';
import webpack from 'webpack';
import webpackStream from 'webpack-stream'
import named from 'vinyl-named';
import * as config from './../config.js';
import * as webpack_config from './../../webpack.config.js';

gulp.task('webpack', () => {
  return gulp.src(config.js.files)
    .pipe(named())
    .pipe(webpackStream(webpack_config.default, webpack))
    .pipe(gulp.dest(config.js.dest));
});
