import gulp from 'gulp';
import * as config from '../config.js';

gulp.task('watch', () => {
  gulp.watch(config.templates.files, ['templates']);
  gulp.watch(config.js.files,        ['eslint', 'webpack']);
  gulp.watch(config.scss.files,      ['sass']);
});
