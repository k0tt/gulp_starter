import gulp from 'gulp'
import ava from 'gulp-ava'
import codecov from 'gulp-codecov'
import dotenv from 'gulp-dotenv'
import * as config from '../config.js'

gulp.task('ava', () => {
    gulp.src(config.js.test.files)
        .pipe(ava(config.js.test.options))
})

gulp.task('coverage', () => {
	gulp.src('./coverage/lcov.info')
	.pipe(dotenv())
	.pipe(codecov())
})

gulp.task('test', ['ava', 'coverage'])
