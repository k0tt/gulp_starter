/**
 * Task: flow
 * Description:
 *     Task for type checking .js code with
 *     facebook flow
 * */
import gulp from 'gulp';
import flow from 'gulp-flowtype';
import * as config from '../config.js';

gulp.task('flow', () => {
    return gulp.src(config.js.files)
        .pipe(flow({
            all: false,
            weak: false,
            killFlow: false,
            beep: true,
            abort: false
        }))
})
