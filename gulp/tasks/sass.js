import gulp from 'gulp';
import sass from 'gulp-sass';
import moduleImporter from 'sass-module-importer';
import * as config from '../config.js';

gulp.task('sass', function () {
    return gulp.src(config.scss.files)
	.pipe(sass({ importer: moduleImporter() }))
        .pipe(gulp.dest(config.scss.dest));
});
