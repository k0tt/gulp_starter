const config = require('./gulp/config.js')
const webpack = require('webpack')
const FlowTypePlugin = require('flowtype-loader/plugin')
const Dotenv = require('dotenv-webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
    context: __dirname + '/' + config.js.src,
    entry: {
        'test' : __dirname + '/' + config.js.main,
        'test.min' : __dirname + '/' + config.js.main
    },
    output: {
        path: __dirname + config.js.dest,
        filename: '[name].js',
        library: config.project_name
    },
    resolve: {
        modules: [
            "node_modules",
            "web_modules",
            "bower_components"
        ],
        extensions: ['.js']
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            _: 'underscore'
        }),
        new Dotenv({
            path: './.env', // if not simply .env
            safe: false // do not require env.example
        }),
        new webpack.DefinePlugin({
            'process.env': {
                // This has effect on the react lib size
                'NODE_ENV': JSON.stringify('production'),
            }
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true
        }),
        new UglifyJsPlugin(),
        new FlowTypePlugin()
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "eslint-loader",
                options: {
                  // eslint options (if necessary)
                }
            },{
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },{ 
                test: /\.html$/, 
                loader: "underscore-template-loader" 
            },{ 
                test: /\.css$/, 
                loader: "style-loader!css-loader" 
            },{ 
                test: /\.jpe?g$|\.gif$|\.png$|\.otf$|\.eot$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$/, 
                loader: "file" 
            }
        ]
     }
 }
