import TestClass from '../../modules/test'
import test from 'ava'

let testClass = new TestClass();

test('should return Hello World regardless of input', t => {
    t.is("Hello world", testClass.testFunction(1,2));
})

test('should compare two numbers', t => {
    t.is(false, testClass.testFunction2(1,2));
})

test('Async tests should work', async t => {
    const a = Promise.resolve('a')

    t.is(await a, 'a')
})


