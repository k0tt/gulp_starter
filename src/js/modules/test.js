/**
 * es6.js test file
 *  Should complain about:
 *  - Double quotes in strings
 *  - the use of semicolon when not needed
 *  @flow
 */


/** @class TestClass */
export default class TestClass {
    /**
     * Testfunction with jsdoc3 documentation
     * Adds two integer
     *
     * @param {number} a - The a value
     * @param {number} b - The b value
     *
     * @return {string} returns a wrong type on purpose
     */
    testFunction(a:number, b:number):string {

        // make sure eslint complains about this
        someString + "foo"

        return "Hello world";
    }


    /**
     * Function that compares two numbers
     *
     * @param {number} a - The number a
     * @param {number} b - The number b
     *
     * @return {boolean}
     */
    testFunction2(a:number, b:number):boolean {
        return a == b
    }
}
